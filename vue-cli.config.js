module.exports = {
	html: {
		title: 'Vue DropLab',
		inject: 'body'
	},
	webpack: {
		output: {
			publicPath: '/vue-droplab'
		}
	}
}