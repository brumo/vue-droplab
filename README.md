# Vue DropLab
This a [Vue](https://vuejs.org/) component implementing [DropLab](https://gitlab.com/gitlab-org/droplab), a library for dropdowns from Gitlab.

**[DEMO](http://brumo.gitlab.io/vue-droplab)**

## Installation


**Step 1:** Import vue-droplab

**A**: Include the single file component **(Recommended)**

```javascript
// import
import droplab from './src/Droplab.vue'

// Or, require
var droplab = require('./src/Droplab.vue')

```
or, **B**: Include distribution files with `<script>` and `<style>`

```html
<script src="yourpath/vue/dist/vue.min.js"></script>
<script src="yourpath/vue-droplab/dist/vue-droplab.min.js"></script>

<link href="yourpath/vue-droplab/dist/vue-droplab.min.css" rel="stylesheet"></link>
```


**Step 2**: Include droplab in your component or globally

```javascript
// global
Vue.component('dropdown', droplab)
// local
var yourComponent = new Vue({
  components: { droplab },
  ...
})
```


**Step 3**: Then, you can introduce the `dropdown` tag anywhere you like in your component's template

```html
<dropdown></dropdown>
```


## Usage

The component has 3 parts:
 - The `id` attribute, which has to be unique for every dropdown instance.
 - The trigger element, which is where the event handlers are attached, you can mark whatever element as a trigger adding the `slot="trigger"` attribute.
 - The dropdown child elements, which are the ones shown when the trigger is clicked, it's recommended to use `<li>` but you can use any markup ( if the component detect any `<li>` as a child, it will automatically wrap the childs in a `<ul>`)

```html
<dropdown id="simple-dropdown">
	<button slot="trigger">Open</button>
	<li>Home</li>
	<li>About</li>
	<li>Contact</li>
</dropdown>
```


### Dynamic data

You can also use the `list` attribute to pass an array of data from any Vue instance into the dropdown.
If you use the `list` attribute you'll need to define a template for the list items, but because of Vue interpolation syntax it's not possible to use droplab's template syntax, so you'll need to define it inside a `v-html` directive, which gets rendered into html without being parsed by Vue. 

```html
<dropdown id="dynamic-dropdown" :list="listData">
	<input type="text" slot="trigger">
	<li v-html="`<a href='#' data-id='{{id}}'>{{text}}</a>`" @click="log"></li>
</dropdown>
```

In this example you can see the use of an `<input>` element as a trigger, and the binding of a `@click` handler for the dropdown items.
